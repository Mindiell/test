---
title: my title
author: my name
date: today
---

% title
% author(s) (separated by semicolons)
% date

# Premier test 1, 2, 3

Ceci est un __test__

Aller directement au [Heading IDs](#custom-id)

t

Visit [Daring Fireball][] for more information.

footnote [1][].

t

I get 10 times more traffic from [Google][] than from
[Yahoo][] or [MSN][].

t

H~2~O

t

t

> :warning: **Warning:** Do not push the big red button.

> :memo: **Note:** Sunrises are beautiful.

> :bulb: **Tip:** Remember to appreciate the little things in life.


Copyright (©) — &copy;

Registered trademark (®) — &reg;

Trademark (™) — &trade;

Euro (€) — &euro;

Left arrow (←) — &larr;

Up arrow (↑) — &uarr;

Right arrow (→) — &rarr;

Down arrow (↓) — &darr;

Degree (°) — &#176;

Pi (π) — &#960;

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |


- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media


| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title |
| Paragraph   | First paragraph. <br><br> Second paragraph. |


X^2^

http://www.example.com

`http://www.example.com`

    http://www.example.com

&nbsp;&nbsp;&nbsp;&nbsp;This is the first sentence of my indented paragraph.

<center>This text is centered.</center>

[This is a comment that will be hidden.]: #

t

t

Here's a ==simple== footnote,[^1] [] and here's a longer one.[^bignote] []

t

t

t

t

t

t

t

t

t

t

t

t

t

t

t

t

t

t

t



### My Great Heading {#custom-id}




* * *


[1]: first footnote.

[^1]: This is the first footnote.

[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.

[Daring Fireball]: http://daringfireball.net/

[google]: http://google.com/        "Google"
[yahoo]:  http://search.yahoo.com/  "Yahoo Search"
[msn]:    http://search.msn.com/    "MSN Search"

